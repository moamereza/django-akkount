=======
Account
=======

This is a django account for custom user model and custom model manager.

===========
Quick start
===========

1. First of all add 'account' to your INSTALLED_APPS settings:
   INSTALLED_APPS = [
      ...
      'account',
  ]

2. Include the account URLconf in your main urls.py project like this:
   path("account/", 'books.urls', namespace="account"),

3. Run ``python manage.py migrate`` to create the account models.

5. Visit http://127.0.0.1:8000/account/ to participate in the poll.
